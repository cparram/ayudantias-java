package tarea2.intro.computacion.uandes;

import java.util.Random;

public class Encomienda {
	/* Atributos. */
	private int peso;
	private int codigo;
	
	/* Constructor de la clase.
	   En este caso, pedido en el enunciado de la tarea
	   los atributos de la encomienda se inicializan
	   aleatoriamente  */
	public Encomienda(){
		/* Random, clase utilizada para generar n�meros
		   aleatorios. */
		Random random = new Random();
		
		/* rando.nextInt(100) retorna un n�mero cualquiera entre
		   0 y 99, por lo que al sumarle 1, los numero aleatorios 
		   est�n entre 1 y 100. */
		this.peso = random.nextInt(100) + 1;
		this.codigo = random.nextInt(300) + 1;
	}
	
	/* M�todo que retorna el peso la encomienda */
	public int obtenerPeso(){
		return peso;
	}
	
	/* M�tod que retorna el c�digo de ciudad de la encomienda */
	public int obtenerCodigo(){
		return codigo;
	}
}
