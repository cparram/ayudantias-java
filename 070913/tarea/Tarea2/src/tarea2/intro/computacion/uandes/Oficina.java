package tarea2.intro.computacion.uandes;

public class Oficina {
	/* Atributos. */
	private Encargado uno;
	private Encargado dos;
	private Encargado tres;
	
	/* Constructor de la clase. */
	public Oficina(Encargado uno, Encargado dos, Encargado tres) {
		this.uno = uno;
		this.dos = dos;
		this.tres = tres;
	}
	
	/* M�todo que verifica el c�digo de la encomienda y se la transfiere
	   al encargado correspondiente. */
	public void recepcionarEncomienda(Encomienda encomienda) {
		/* C�digo de ciudad de la encomienda. */
		int codigo = encomienda.obtenerCodigo();
		
		System.out.println("Oficina de Recepci�n: Encomienda recibida. Peso "
				+ encomienda.obtenerPeso() + " kg con destino " + codigo);

		if (codigo <= 100) {
			uno.administrar(encomienda);
		} else if (codigo >= 101 && codigo <= 200) {
			dos.administrar(encomienda);
		} else {
			tres.administrar(encomienda);
		}

	}
}
