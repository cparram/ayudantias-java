package tarea2.intro.computacion.uandes;

public class Camion {
	/* Atributos. */
	private int capacidad;
	private int carga;
	
	/* Constructor de la clase.
	   Ac� se establece la capacidad que tiene el cami�n
	   para trasportar las cajas de encomiendas y la carga
	   inicial que debe ser cero. */
	public Camion(int capacidad, int carga){
		this.capacidad = capacidad;
		this.carga = carga;
	}
	
	/* M�todo que retorna el estado del cami�n,
	   si est� lleno o no (si su carga es mayor o igual
	   que su capacidad). */
	public boolean estaLleno(){
		if (carga >= capacidad){
			return true;
		}
		else {
			return false;
		}
	}
	
	/* M�todo que carga al cami�n el peso de una nueva caja */
	public void cargar(int nuevaCarga) {
		carga += nuevaCarga; 
		System.out.println("Cami�n: Peso total de carga " +  carga + " kg.");
	}
	
	/* M�todo que se llama cuando ya est�n todas las cajas cargadas */
	public void distribuir() {
		System.out.println("Cami�n: Iniciando distribuci�n");
		
	}
}
