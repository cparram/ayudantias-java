package tarea2.intro.computacion.uandes;

public class Caja {
	/* Atributos. */
	private int peso;
	
	/* Constructor.
	   Inicialmente una caja siempre est� vac�a. */
	public Caja(){
		this.peso = 0;
	}
	
	/* M�todo que agrega una encomienda a la caja.
	   M�todo principalemente llamado por el encargado
	   que recibe la encomienda. */
	public void agregarEncomienda(Encomienda encomienda){
		peso += encomienda.obtenerPeso();
	}
	
	/* M�todo que retorna el peso de la caja */
	public int obtenerPeso() {
		return peso;
	}
}
