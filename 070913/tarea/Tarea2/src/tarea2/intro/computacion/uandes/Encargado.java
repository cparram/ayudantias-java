package tarea2.intro.computacion.uandes;

public class Encargado {
	/* Atributos. */
	private Caja caja;
	private String nombre;
	private Camion camion;
	
	/* Constructor de la clase Encargado.
	   El nombre nos sirve para indentificar a cada encargado para
	   luego mostrarlo por consola.
	   El cami�n es donde cargar� la caja cuando est� llena 
	   (mayor a 100 kg).
	 */
	public Encargado(String nombre, Caja caja, Camion camion) {
		this.caja = caja;
		this.nombre = nombre;
		this.camion = camion;
	}
	
	/* M�todo que recibe una encomienda y la agrega a la caja.
	   Luego verifica si la caja estar�a lista para sellarla. */
	public void administrar(Encomienda encomienda) {
		System.out.println(nombre + ": Encomienda de "
				+ encomienda.obtenerPeso() + " kg con destino "
				+ encomienda.obtenerCodigo() + " en caja");
		
		/* Agrega encomienda a la caja. */
		caja.agregarEncomienda(encomienda);
		if (caja.obtenerPeso() > 100) {
			
			/* Si la caja supera el peso establecido
			   se sella y se carga. */
			sellar();
		}
	}
	
	/* M�todo que lleva a cabo el sellado de la caja para
	   luego cargarla al cami�n si es el caso. */
	public void sellar() {
		
		/* Puede ser que la caja tenga peso igual a cero.
		   Este caso se presenta siempre que el cami�n est� lleno
		   al final de la ejecuci�n del programa */
		if (caja.obtenerPeso() == 0) {
			System.out.println(nombre + ": Caja vac�a. Nada que cargar.");
		} else {
			System.out.println(nombre + ": Sellando caja de "
					+ caja.obtenerPeso() + " kg. Cargando cami�n");
			/* carga la caja en el cami�n */
			cargar();
		}
	}
	
	/* M�tod que carga el peso de la caja en el camion */
	public void cargar() {
		camion.cargar(caja.obtenerPeso());

		/* Al cargar la caja ahora debe empezar de nuevo pero 
		   con una caja vac�a. */
		caja = new Caja();
	}
}
