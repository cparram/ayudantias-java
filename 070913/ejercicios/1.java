class Funcion {
	/* Atributos que tendr� cada funci�n, es decir,
	   cada instancia de la clase Funci�n. */
	private String nombre;
	private int precio;
	
	/* Constructor. Inicializa los atributos
	   de cada clase. */
       	public Funcion(String nombre, int precio){
       		this.nombre = nombre;
		this.precio = precio;
       	}

	/* M�todo que cambia el nombre de la funci�n. */
	public void cambiarNombre(String nuevoNombre){
		this.nombre = nuevoNombre;
	}

	/* M�todo que cambia el precio de la funci�n */
	public void cambiarPrecio(int nuevoPrecio){
		this.precio = nuevoPrecio;
	}
}

class Teatro {
	/* Atributos que tendr� cada instancia de la
	   clase Teatro. */
	private String nombre;
	private String direccion;

	// Cada instancia debe tener 4 funciones,
	// todas distintas.
	private Funcion primera;
	private Funcion segunda;
	private Funcion tercera;
	private Funcion cuarta;

	/* Constructor. Inicializa todos los atributos 
	   que tendr� cada instancia. */
	public Teatro(String nombre, String direccion,
			Funcion primera, Funcion segunda,
			Funcion tercera, Funcion cuarta){
		this.nombre = nombre;
		this.direccion = direccion;
		this.primera = primera;
		this.segunda  = segunda;
		this.tercera = tercera;
		this.cuarta = cuarta;
	}

	/* M�todo que cambia el nombre del teatro. */
	public void cambiarNombre(String nuevoNombre){
		this.nombre = nuevoNombre
	}
}

class Programa {
	public void static Main(String[] args){
		/* Se crean cuatro instancias de la clase
		   Funcion, todas distintas. */
		Funcion f1 = new Funcion("Amores de cantina", 5000);
		Funcion f2 = new Funcion("V�ctor sin V�ctor Jara", 6000);
		Funcion f3 = new Funcion("Batalla de sencillez", 6000);
		Funcion f4 = new Funcion("Comedia y sue�o", 4000);

		/* Se crea una instancia de la clase Teatro. */
		Teatro gam = new Teatro("GAM",
				"Av. Libertador Bernardo O�Higgins 227",
			       	f1, f2, f3, f4);
		
		/* Se cambia el nombre de la tercera funci�n */
		f3.cambiarNombre("Romeo&Julieta");
		f3.cambiarPrecio(10000);

		gam.cambiarNombre("Centro Cultural Gabriela Mistral");
	}
}
