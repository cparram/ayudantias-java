package tarea2.intro.computacion.uandes;

public class Main {

	public static void main(String[] args) {
		
		/* Tres distintas instancias de la clase Caja. */
		Caja vacia1 = new Caja();
		Caja vacia2 = new Caja();
		Caja vacia3 = new Caja();
		
		/* Cami�n donde se cargar�n las cajas. */
		Camion camion = new Camion(14500, 0);
		
		/* Cada encargado tiene una caja distinta en donde van 
		   poniendo las encomiendas, pero los tres cargan la caja
		   en el mismo cami�n.
		   Para identificar a cada encargado, inicialmente se les
		   da un nombre. */
		Encargado uno = new Encargado("Encargado 1", vacia1, camion);
		Encargado dos = new Encargado("Encargado 2", vacia2, camion);
		Encargado tres = new Encargado("Encargado 3", vacia3, camion);
		
		/* Oficina donde se recibir�n las encomiendas.
		   Los encargados regionales trabajan dentro de la oficina. */
		Oficina oficina = new Oficina(uno, dos, tres);
		
		/* Mientras el cami�n no est� lleno, la oficina debe recibir
		   las encomiendas. */
		while (!camion.estaLleno()){
			
			/* Una nueva encomienda que se crea en cada 
			   ciclo del while. */
			Encomienda encomienda = new Encomienda();
			
			/* Oficina recepciona la encomienda y se la transfiere 
			   al encargado correspondiente. */
			oficina.recepcionarEncomienda(encomienda);
		}
		
		/* Luego que el cami�n supere cierto umbral de peso
		   se debe cargar las cajas de los encargados que falten. */
		
		uno.sellar();
		dos.sellar();
		tres.sellar();
		
		/* Ya con todas las cajas cargadas, comienza la distribuci�n. */
		camion.distribuir();
	}

}
